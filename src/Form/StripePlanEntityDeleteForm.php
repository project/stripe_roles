<?php

namespace Drupal\stripe_roles\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Stripe plan entities.
 *
 * @ingroup stripe_roles
 */
class StripePlanEntityDeleteForm extends ContentEntityDeleteForm {
}
