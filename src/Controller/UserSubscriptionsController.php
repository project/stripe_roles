<?php

namespace Drupal\stripe_roles\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\stripe_roles\StripeRolesService;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserSubscriptionsController.
 *
 * @package Drupal\stripe_roles\Controller
 */
class UserSubscriptionsController extends ControllerBase {

  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  protected $request;

  /**
   * Drupal\stripe_roles\StripeRolesService definition.
   *
   * @var \Drupal\stripe_roles\StripeRolesService
   */
  protected $stripeApi;
  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(Request $request, StripeRolesService $stripe_api, EntityTypeManagerInterface $entityTypeManager) {
    $this->request = $request;
    $this->stripeApi = $stripe_api;
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('stripe_roles.stripe_api'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * SubscribeForm.
   *
   * @return array
   *   Return SubscribeForm.
   */
  public function subscribeForm() {
    $form = $this->formBuilder()->getForm('Drupal\stripe_roles\Form\StripeSubscribeForm');

    return $form;
  }

  /**
   * Cancel subscription.
   */
  public function cancelSubscription() {

    $remote_id = $this->request->get('remote_id');
    $user_id = $this->request->get('user');

    try {
      $this->stripeApi->cancelRemoteSubscription($remote_id);
      $local_subscription = $this->stripeApi->loadLocalSubscription(['subscription_id' => $remote_id]);
      // Setup the status of a local subscription in 'cancel'.
      // As the Stripes changes the status after some time wee will not to
      // synchronize local subscription with a remote subscription.
      $local_subscription->set('status', 'canceled');
      // Roles related with a subscription will be removed after calling save()
      // method, because saving initiates a call of the 'updateUserRoles()'
      // method of a local subscription entity.
      $local_subscription->save();
    }
    catch (\Exception $e) {
    }

    // Do not do the redirect to the 'Subscription Plans' because it will be
    // done with the 'Rules' module.
    // The redirect may depend of a plan that user have unsubscribed.
    return [];
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   TRUE if the user is allowed to cancel the subscription.
   */
  public function accessCancelSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

  /**
   * Reactivate subscription.
   */
  public function reactivateSubscription() {
    $remote_id = $this->request->get('remote_id');

    $this->stripeApi->reactivateRemoteSubscription($remote_id);
    $this->stripeApi->syncRemoteSubscriptionToLocal($remote_id);

    return $this->redirect("stripe_roles.user.subscriptions.viewall", [
      'user' => $this->currentUser()->id(),
    ]);
  }

  /**
   * Checks access for a specific request.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   Run access checks for this account.
   *
   * @return \Drupal\Core\Access\AccessResult
   *   TRUE if the user is allowed to reactivate a subscription.
   */
  public function accessReactivateSubscription(AccountInterface $account) {
    $remote_id = \Drupal::request()->get('remote_id');

    return AccessResult::allowedIf($account->hasPermission('administer stripe subscriptions') ||
      ($account->hasPermission('manage own stripe subscriptions') && $this->stripeApi->userHasStripeSubscription($account, $remote_id)));
  }

}
