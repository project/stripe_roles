<?php

namespace Drupal\stripe_roles\EventSubscriber;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\stripe_api\Event\StripeApiWebhookEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\stripe_roles\StripeRolesService;

/**
 * Class WebHookSubscriber.
 *
 * @package Drupal\stripe_roles
 */
class WebHookSubscriber implements EventSubscriberInterface {

  /**
   * Drupal\stripe_roles\StripeRolesService definition.
   *
   * @var \Drupal\stripe_roles\StripeRolesService
   */
  protected $stripeApi;

  /**
   * The logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * WebHookSubscriber constructor.
   *
   * @param \Drupal\stripe_roles\StripeRolesService $stripe_roles_stripe_api
   *   Stripe roles service.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   Logger.
   */
  public function __construct(StripeRolesService $stripe_roles_stripe_api, LoggerChannelInterface $logger) {
    $this->stripeApi = $stripe_roles_stripe_api;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events['stripe_api.webhook'][] = ['onIncomingWebhook'];
    return $events;
  }

  /**
   * Process an incoming webhook.
   *
   * @param \Drupal\stripe_api\Event\StripeApiWebhookEvent $event
   *   Logs an incoming webhook of the setting is on.
   */
  public function onIncomingWebhook(StripeApiWebhookEvent $event) {
    $type = $event->type;
    $data = $event->data;
    $stripe_event = $event->event;

    if (\Drupal::config('stripe_api.settings')->get('log_webhooks')) {
      $this->logger->info("Event Subscriber reacting to @type event:\n @event",
        ['@type' => $event->type, '@event' => (string) $stripe_event]);
    }

    // React to subscription life cycle events.
    // @see https://stripe.com/docs/subscriptions/lifecycle
    switch ($type) {
      // Occurs whenever a customer with no subscription is signed up for plan.
      case 'customer.subscription.created':
        break;

      // Occurs whenever a invoice failed.
      case 'invoice.payment_failed':
        // Remove roles after 2 attempt.
        if ($data->object->attempt_count == 2) {
          try {
            $local_subscription = $this->stripeApi->loadLocalSubscription(['subscription_id' => $remote_id]);
            // Setup the status of a local subscription in 'cancel'.
            // As the Stripes changes the status after some time wee will not to
            // synchronize local subscription with a remote subscription.
            $local_subscription->set('status', 'canceled');
            // Roles related with a subscription will be removed after calling save()
            // method, because saving initiates a call of the 'updateUserRoles()'
            // method of a local subscription entity.
            $local_subscription->save();
          }
          catch (\Exception $e) {
          }
        }
        break;

      // Occurs whenever a customer ends their subscription.
      case 'customer.subscription.deleted':
        $remote_subscription = $data->object;
        try {
          // In our business case we don't need to delete canceled subscription.
          // Roles related with a canceled subscription will be removed after
          // syncRemoteSubscriptionToLocal() by calling updateUserRoles()
          // method of "stripe_subscription" entity (local subscription).
          /** @var \Drupal\stripe_roles\Entity\StripeSubscriptionEntity $local_subscription */
          $this->stripeApi->syncRemoteSubscriptionToLocal($remote_subscription->id);
        }
        catch (\Throwable $e) {
          $this->logger->error("Failed to delete local subscription: @exception", ['@exception' => $e->getMessage()]);
        }
        break;

      // Occurs three days before the trial period of a subscription is
      // scheduled to end.
      case 'customer.subscription.trial_will_end':
        break;

      // Occurs whenever a subscription changes. Examples would include
      // switching from one plan to another, or switching status from trial
      // to active.
      case 'customer.subscription.updated':
        break;
    }

  }

}
